# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2020 Tomaz Canabrava <tcanabrava@kde.org>

find_package(Qt${QT_MAJOR_VERSION} ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Quick
    Xml
)

find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    I18n
    Declarative
    Auth
    Config
)


add_library(networkstatus STATIC)
set_property(TARGET networkstatus PROPERTY POSITION_INDEPENDENT_CODE ON)
target_sources(networkstatus PRIVATE
    conectionsmodel.cpp
    conectionsmodel.h
    netstatclient.cpp
    netstatclient.h
    netstathelper.cpp
    netstathelper.h
)

target_link_libraries(networkstatus
    kcm_firewall_core
    Qt::Core
    Qt::Quick
    KF5::CoreAddons
    KF5::ConfigCore
    KF5::AuthCore
    KF5::I18n
)
